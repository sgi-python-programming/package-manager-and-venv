import camelcase
from urllib3 import request

c = camelcase.CamelCase()
txt = "hello world"

print(c.hump(txt))

resp = request("GET", "http://httpbin.org/robots.txt")
result = resp.data
print(result.decode('utf-8'))